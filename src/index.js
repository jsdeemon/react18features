import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import reportWebVitals from './reportWebVitals';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// possibility to make root.unmount()

// for Server Side rendering we chenage ReactDOM.createRoot to ReactDOM.hydrateRoot()
// гидрация - это когда мы получили с севреа готовую HTMLю и уже на нее накручиваем эффекты, слушатели события и пр 

// This new API is now exporting from ceact-dom/client 

// For SERVER 
// SE Modules 
// imoort ReactDOMServer from 'react-dom/server'
// commonJs 
// var ReactDOMServer = require('react-dom/server') 

/*
ДОбавляется две новых функции для работы со стримами
renderToPipebableStream: for streaming in Node environments 
renderToReadabelsStream: for modern edge runtime environments. such as Deno and Cloudflare workers 

ранее использовался renderToString 


*/


// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
