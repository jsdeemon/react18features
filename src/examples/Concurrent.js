import React, {useMemo, useState, useTransition} from 'react'


const Concurrent = () => {


    const filterItems = []

    const [value, setValue] = useState('')
    const [items, setItems] = useState('')

    // filtered value 
    const [filteredValue, setFilteredValue] = useState('')
    // using Transition API
    const [isPending, startTransition] = useTransition()

    const filteredItems = useMemo(() => {
        return items.filter(item => item.title.toLowerCase().includes(filteredValue))
    }, [filteredValue]) 

    const onChangeValue = (e) => {
        setValue(e.target.value) 

        startTransition(() => {
            setFilteredValue(e.target.value)
        })
    }

  return (
    <>
    <input type="text" value={value} onChange={onChangeValue} /> 
    {isPending && <h1>Loading ...</h1>}
    <div>
        {filterItems.map(item => (
            <div key={item.id}>
                <div>id = {item.id}</div>
                <div>{item.title}</div>
            </div>
        )
        )}
    </div>
    </>
  )
}

export default Concurrent