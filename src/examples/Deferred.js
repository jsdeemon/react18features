import React, {useDeferredValue, useMemo, useState} from 'react'

const Deferred = () => {

    const filterItems = []

    const [value, setValue] = useState('')
    const [items, setItems] = useState('')

    // filtered value 
    const [filteredValue, setFilteredValue] = useState('')
    // using Deferred value hook 
    const deferredValue = useDeferredValue(value)

    const filteredItems = useMemo(() => {
        return items.filter(item => item.title.toLowerCase().includes(deferredValue))
    }, [deferredValue]) 

    const onChangeValue = (e) => {
        setValue(e.target.value) 
    }


  return (
    <>
    <input type="text" value={value} onChange={onChangeValue} /> 
    <div>
        {filterItems.map(item => (
            <div key={item.id}>
                <div>id = {item.id}</div>
                <div>{item.title}</div>
            </div>
        )
        )}
    </div>
    </>
  )
}

export default Deferred