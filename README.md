# ReactJs 18 features 

### Batching 

```javascript
const update = () => {
    setValue(prev => prev + 1)
    setValue(prev => prev + 1)
} // state changed twice, but rerender only once 
```
Batching in React 17 did not worked with async events: 
```javascript
const update = () => {
    setTimeout(() => {
       setValue(prev => prev + 1)
       setValue(prev => prev + 1)
    }, 2000)
}

const update = () => {
   fetch(`http://localhost:5000`)
     .then(() => {
          setValue(prev => prev + 1)
          setValue(prev => prev + 1)
     })
}
```
### Concurrent 
Конкурентный режим позволяет сделать перерисовку основного интерфейса отложенной, то есть он сам строит очередь приоритетов и сам определяет, когда эту прорисовку произвести 
При этом на интерфейса это никак не отразится 

### Transitions - позволяет сделать отложенный рендер 

```javascript
import {starttransition} from 'react`

// Urgent: what was typed
setInputValue(input)

// mark any state updates inside as transitions 
startTransition(() => {
    // Transition: show the results
    setSearchQuery(input)y
})

```

### Хук useDeferredValue(value)
Отложенно изменяет значение 
Позволяет делать отложенную перерисовку компонентов 

### Хук useId 
Возвращает уникальное значение id 

### Хук useSyncExternalStore
Позволяет интегрироваться с состоянием, которое внешне по отношению к реакту. Также позволяет внешнему хранилищу работать с функциями параллельного рендеринга 
```javascript
const state = useSyncExternalStore(subscribe, getSnapshot[, getServerSnapshot])
const selectedField = useSyncExternalStore(
    store.subscribe, 
    () => store.getSnapshot().selectedField,
)
```

### Хук useInsertionEffect
```javascript
useInsertionEffect(didUpdate)
```
Похож на useEffect, но срабатывает синхронно перед всеми мутациями DOM 
Он нужен только для библиотек и для библиотек, которые работают с CSS внутри Javascript, например styled components 

### lazy и Suspense 
Можно использовать на сервере (ssr)
раньше нужно было использовать loadable components 
```javascript
import loadable from '@loadable/component'

const OtherComponent = loadable(() => import('./OtherComponent))
```
Сейчас можно использовать нативный lazy 
```javascript
const OtherComponent = React.lazy(() => import('./OtherComponent))

function MyComponent() {
    return (
        <div>
          <Suspense fallback={<div>Loading...</div>}>
          <OtherComponent />
          </Suspense>
        </div>
    )
}
``` 
Нативные решения всегда лучше

### <Offscreen>
Сохранение состояния компонента при удалении / появлении 

### Server components - still in Developemt
Позволяют лучше работать конкурентному режиму, потоковой передачи данных, suspense и пр. 
будут нужны для создателей фреймворков на базе ReactJs